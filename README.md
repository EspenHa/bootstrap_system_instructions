1. Install packages
	1. pacman
		1. `sudo pacman -Syyu`
		1. `sudo pacman -S --needed - < dev_pkglist.txt`
		1. `sudo pacman -S --needed cudnn python-pytorch-cuda python-tensorflow-cuda`, if wanted
	1. AUR
		1. `yay -Syy`
		1. `yay -S --needed - < aur_pkglist.txt`
	1. pip
		1. `sudo pip2 install bitbucket-cli`, for push to bitbucket script
		1. `sudo pip2 install pynvim`, for nvim plugins
1. Set yadm class to `laptop` or `desktop`, see https://yadm.io/docs/alternates
	*  `export YADM_CLASS=<class>`
1. Clone yadm settings


		# Enable smartcard service for yubikey
		sudo systemctl enable --now pcscd.service

		# Import public key
		curl https://keybase.io/espenha/key.asc | gpg --import
		echo -e "5\ny\n" | gpg --command-fd 0 --edit-key 0xACEEDE9AB18897FD trust quit

		# Export public key from yubikey
		export GPG_TTY="$(tty)"
		export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

		# gpg-connect-agent updatestartuptty /bye
		# or
		# gpgconf --launch gpg-agent
		# might or might not be needed

		ssh-add -L > /tmp/id_rsa_yubikey.pub

		GIT_SSH_COMMAND='ssh -i /tmp/id_rsa_yubikey.pub' yadm clone --no-bootstrap git@bitbucket.org:EspenHa/dotfiles_yadm.git
		GIT_SSH_COMMAND='ssh -i /tmp/id_rsa_yubikey.pub' sudo -E yadm clone -w / --no-bootstrap git@bitbucket.org:EspenHa/root_settings_yadm.git

		sudo yadm config local.class $YADM_CLASS
		yadm config local.class $YADM_CLASS

		sudo yadm bootstrap
		yadm bootstrap

1. Install oh-my-zsh
	1. `git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh`
	1. `yadm checkout -- ~/.oh-my-zsh/custom/plugins/yadm/yadm.plugin.zsh`
1. Change shell to zsh
	* `sudo chsh -s /usr/bin/zsh $USER`
1. Install nvim plugins
	1. `curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
		https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim`
	1. `nvim --headless +PlugInstall +qa`
	1. `nvim +checkhealth`
1. Create mount dirs
	* `mkdir -p /media/Common`
1. Install dropbox, if wanted
	1. `mkdir -p /media/Common/Dropbox`
	1. `ln -s /media/Common/Dropbox`
	1. `yay -S --needed dropbox`
1. Add to fuse group, for sshfs
	1. `groupadd fuse`
	1. `usermod -a -G fuse $USER`
1. Start systemd services
	* Old ssh agent (not needed if using gpg agent)
		* `systemctl --user enable --now ssh-agent`
	* Reverse ssh tunnel
		* `sudo systemctl enable --now reverse_ssh_tunnel.service`
1. Enable bumblebee, if wanted
	* `sudo pacman -S bumblebee`
	* `sudo gpasswd -a $USER bumblebee`
	* `sudo mhwd -i pci video-hybrid-intel-nvidia-bumblebee`
	* `sudo systemctl enable --now bumblebeed.service`
1. Enable mining, if wanted
	* `sudo systemctl enable --now ETHlargementPill.service`
	* `sudo systemctl enable --now mine_eth.service`
	* `sudo systemctl enable --now overclock_noserver_GPU.service`
